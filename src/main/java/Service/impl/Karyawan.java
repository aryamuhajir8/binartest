package Service.impl;

import Service.ServiceKarywan;

public class Karyawan implements ServiceKarywan {
    static String[] karyawan = {"doni", "dino", "diki", "dodi","dudung"};
    static Integer[] statusKaryawan = {1,2, 3, 4,5};
    static String[] karyawanDelete = new String[karyawan.length - 1];
    static String[] karyawanTambah = new String[karyawan.length + 1];
    Integer[] status;

    @Override
    public String[] lihatKaryawan() {
        return karyawanTambah;
    }

    @Override
    public int cekStatus(String nama) {
        int indexKaryawan = 0;
        for (int i = 0; i<karyawan.length; i++){
            if (karyawan[i] == nama){
                indexKaryawan =i;
            }

        }
        if (indexKaryawan == 1){
            System.out.println(nama + " Mendapatkan bonus sebesar 10jt");
        }else if (indexKaryawan == 2){
            System.out.println(nama + " Mendapatkan bonus sebesar 20jt");

        }else if (indexKaryawan == 3){
            System.out.println(nama + " Mendapatkan bonus sebesar 30jt");

        }else {
            System.out.println(nama + " Mendapatkan bonus sebesar 50jt");

        }
        return indexKaryawan;
    }

    @Override
    public String[] lihatKaryawanTerbaru() {
        return karyawanDelete;
    }

    @Override
    public void addKaryawan(String nama) {
        String karyawanSementara;
        for (int i = 0, k = 0; i<karyawan.length; i++){
            karyawanTambah[i] = karyawan[i];

        }
        karyawanTambah[karyawan.length] = nama;
    }

    @Override
    public void loopStatus(int grade) {
        if (grade == 1){
            for (int i = 0; i<= 10; i++){
                System.out.println(i);
            }
        }else if (grade == 2){
            for (int i = 0; i<= 10; i++){
                System.out.println(i);

                if (i == 5){
                    break;
                }
            }

        }else {
            for (int i = 0; i<= 10; i++){
                if (i == 8){
                    continue;
                }else{
                    System.out.println(i);
                }
            }
        }

    }


    @Override
    public void deleteKaryawan(String nama) {
        String karyawanSementara;
        for (int i = 0, k = 0; i<karyawan.length; i++){
            karyawanSementara = karyawan[i];
            if (karyawanSementara.equals(nama) ){

            }else {
                karyawanDelete[k++] = karyawan[i];

            }
        }

    }
}
